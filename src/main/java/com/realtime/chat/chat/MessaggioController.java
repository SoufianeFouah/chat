package com.realtime.chat.chat;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.realtime.chat.dao.MessaggioDao;
import com.realtime.chat.dao.UtenteDao;
import com.realtime.chat.model.Messaggio;
import com.realtime.chat.model.Utente;

@RestController
@CrossOrigin("Http://localhost:8083")
@RequestMapping("/Messaggio")
public class MessaggioController {
	
	@GetMapping("/list")
	public ArrayList<Messaggio> restituisciMessaggi(){
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
		
		MessaggioDao messaggi = new MessaggioDao();
		
		try { 
			elenco = messaggi.getAll();
			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());	
		}
		
		return elenco;
	}
	
	@PostMapping("/insert")
	public Messaggio insMessaggio(@RequestBody Messaggio messaggi) {
		Messaggio temp = null;
		MessaggioDao nuovomessaggio = new MessaggioDao();
		try {
			nuovomessaggio.insert(messaggi);
			temp = messaggi;
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
	}
	
	@DeleteMapping("/delete/{idMessaggio}") 
	public boolean deleteMessaggio(@PathVariable Integer idMessaggio) {
		boolean delete = false;
		MessaggioDao messaggi = new MessaggioDao();
		try {
			Messaggio temp = messaggi.getById(idMessaggio);
			delete = messaggi.delete(temp);
			
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		
		return delete;
	}

}
