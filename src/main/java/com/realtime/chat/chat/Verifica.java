package com.realtime.chat.chat;

import java.sql.SQLException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.realtime.chat.dao.UtenteDao;
import com.realtime.chat.model.Utente;

@RestController
@CrossOrigin("Http://localhost:8080")
@RequestMapping("/verifica")
public class Verifica {
	
	@PostMapping("/login") 
	public Utente LoginCorretto (@RequestBody Utente utente) {
		Utente temp=null;
		UtenteDao utenti = new UtenteDao();
		
		try {
			temp= utenti.trovaUtente(utente.getUsername(), utente.getPasswordUtente());
					
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		if (temp != null) {
			
			return temp;
		}
		return null;
	}
	
	@PostMapping("/registrazione")
	public boolean RegistrazioneCoretta (@RequestBody Utente utente) {
		UtenteDao utenti = new UtenteDao();
		
		try {
			utenti.insert(utente);
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			
			return false;
	    }
			return true;
	}
		
}
