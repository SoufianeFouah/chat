package com.realtime.chat.chat;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.realtime.chat.dao.UtenteDao;
import com.realtime.chat.model.Utente;

@RestController
@CrossOrigin("Http://localhost:8083")
@RequestMapping("/Utente")
public class UtenteController {

	@GetMapping("/list")
	public ArrayList<Utente> restituisciUtenti(){
		ArrayList<Utente> elenco = new ArrayList<Utente>();
		
		UtenteDao utenti = new UtenteDao();
		
		try { 
			elenco = utenti.getAll();
			
		} catch (SQLException e) {
			
			System.out.println(e.getMessage());	
		}
		
		return elenco;
	}
	
	@GetMapping("/{username}")
	public Utente getUtente(@PathVariable String username) {
		Utente temp = null;
		UtenteDao utenti = new UtenteDao();
		try {
			temp = utenti.getByUsername(username);
					
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			
		}
		
		return temp;
	}
	
	@PostMapping("/insert")
	public Utente insUtente(@RequestBody Utente utente) {
		Utente temp = null;
		UtenteDao utenti = new UtenteDao();
		try {
			utenti.insert(utente);
			temp = utente;
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return temp;
	}
	
	@DeleteMapping("/delete/{username}") 
	public boolean deleteUtente(@PathVariable String username) {
		boolean delete = false;
		UtenteDao utenti = new UtenteDao();
		try {
			Utente temp = utenti.getByUsername(username);
			delete = utenti.delete(temp);
			
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		
		return delete;
	}
	
	
	@PutMapping("/update/{username}")
	public Utente updateUtente(@PathVariable String username, @RequestBody Utente passato) {
		Utente temp = null;
		UtenteDao utenti = new UtenteDao();
		try {
			Utente utente = utenti.getByUsername(username);
			utente.setUsername(passato.getUsername());
			utente.setPasswordUtente(passato.getPasswordUtente());
			utente.setTipologia(passato.getTipologia());
			temp = utenti.update(utente);
			temp.setIdUtente(null);
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());}
		
		return temp;
	}
}
