package com.realtime.chat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.realtime.chat.connessione.ConnettoreDB;
import com.realtime.chat.model.Messaggio;
import com.realtime.chat.model.Utente;

public class MessaggioDao implements Dao<Messaggio>  {

	@Override
	public Messaggio getById(Integer id) throws SQLException {
		
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idMessaggio, utenteId, textMessaggio, dataMessaggio FROM messaggio WHERE idMessaggio = ?";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet resultato = ps.executeQuery();
		resultato.next();
		Messaggio temp = new Messaggio();
		
		temp.setIdMessaggio(resultato.getInt(1));
		temp.setUtenteId(resultato.getInt(2));
		temp.setTextMessaggio(resultato.getString(3));
		temp.setDataMessaggio(resultato.getTimestamp(4).toLocalDateTime() );

		return temp;
	}

	@Override
	public ArrayList<Messaggio> getAll() throws SQLException {
		ArrayList<Messaggio> elenco = new ArrayList<Messaggio>();
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idMessaggio, utenteId, textMessaggio, dataMessaggio FROM messaggio";
		PreparedStatement ps = connettore.prepareStatement(query);
		ResultSet resultato = ps.executeQuery();
		while (resultato.next()) {
			Messaggio temp = new Messaggio();
			temp.setIdMessaggio(resultato.getInt(1));
			temp.setUtenteId(resultato.getInt(2));
			temp.setTextMessaggio(resultato.getString(3));
			temp.setDataMessaggio(resultato.getTimestamp(4).toLocalDateTime());
			elenco.add(temp);
			}
		return elenco;
		
	}

	@Override
	public void insert(Messaggio t) throws SQLException {
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "INSERT INTO messaggio (utenteId, textMessaggio, dataMessaggio) VALUES (?,?,?)";
		PreparedStatement ps = connettore.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, t.getUtenteId());
		ps.setString(2, t.getTextMessaggio());
		ps.setObject(3, t.getDataMessaggio());
		
		ps.executeUpdate();
		ResultSet resultato = ps.getGeneratedKeys();
		resultato.next();
		
       	t.setIdMessaggio(resultato.getInt(1));

	}

	@Override
	public boolean delete(Messaggio t) throws SQLException {
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM messaggio WHERE idMessaggio = ?";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setInt(1, t.getIdMessaggio());
		int eleminate = ps.executeUpdate();
		if (eleminate > 0 ){
			return true;
		}
		
		return false;
	}

	@Override
	public Messaggio update(Messaggio t) throws SQLException {
		
		return null;
		
	}
	
	
}
