package com.realtime.chat.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.realtime.chat.connessione.ConnettoreDB;
import com.realtime.chat.model.Utente;

public class UtenteDao implements Dao<Utente>{

	public Utente getByUsername(String username) throws SQLException {
		
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idUtente, username, passwodUtente, tipologia FROM utente WHERE username = ?";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setString(1, username);
		ResultSet resultato = ps.executeQuery();
		resultato.next();
		Utente temp = new Utente();
		temp.setIdUtente(resultato.getInt(1));
		temp.setUsername(resultato.getString(2));
		temp.setPasswordUtente(resultato.getString(3));
		temp.setTipologia(resultato.getString(4));
		
		return temp;
	}
	
	public Utente trovaUtente(String username, String passwordUtente) throws SQLException{
		Connection connettore= ConnettoreDB.getIstanza().getConnessione();
		String query= "SELECT idUtente, username, passwodUtente, tipologia FROM utente WHERE username = ? AND passwordUtente= ?";
		PreparedStatement ps= connettore.prepareStatement(query);
		ps.setString(1, username);
		ps.setString(2, passwordUtente);
		ResultSet resultato = ps.executeQuery();
		resultato.next();
		Utente temp = new Utente();
		temp.setIdUtente(resultato.getInt(1));
		temp.setUsername(resultato.getString(2));
		temp.setPasswordUtente(resultato.getString(3));
		temp.setTipologia(resultato.getString(4));
		
		return temp;
		
	}
	
	@Override
	public Utente getById(Integer id) throws SQLException {
		
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idUtente, username, passwodUtente, tipologia FROM utente WHERE idUtente = ?";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet resultato = ps.executeQuery();
		resultato.next();
		Utente temp = new Utente();
		temp.setIdUtente(resultato.getInt(1));
		temp.setUsername(resultato.getString(2));
		temp.setPasswordUtente(resultato.getString(3));
		temp.setTipologia(resultato.getString(4));
		
		return temp;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elenco = new ArrayList<Utente>();
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idUtente, PasswordUtente, tipologia FROM utente";
		PreparedStatement ps = connettore.prepareStatement(query);
		ResultSet resultato = ps.executeQuery();
		while (resultato.next()) {
			Utente temp = new Utente();
			temp.setIdUtente(resultato.getInt(1));
			temp.setUsername(resultato.getString(2));
			temp.setPasswordUtente(resultato.getString(3));
			temp.setTipologia(resultato.getString(4));
			elenco.add(temp);
			}
		return elenco;
		
	}

	@Override
	public void insert(Utente t) throws SQLException {
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "INSERT INTO utente (username, passwordUtente, tipologia) VALUES (?,?,?)";
		PreparedStatement ps = connettore.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getUsername());
		ps.setString(2, t.getPasswordUtente());
		ps.setString(3, t.getTipologia());
		
		ps.executeUpdate();
		ResultSet resultato = ps.getGeneratedKeys();
		resultato.next();
		
       	t.setIdUtente(resultato.getInt(1));

	}

	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM utente WHERE idUtente = ?";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setInt(1, t.getIdUtente());
		int eleminate = ps.executeUpdate();
		if (eleminate > 0 ){
			return true;
		}
		
		return false;
	}

	@Override
	public Utente update(Utente t) throws SQLException {
		Connection connettore = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE utente SET username = ? , password = ? ";
		PreparedStatement ps = connettore.prepareStatement(query);
		ps.setString(1, t.getUsername());
		ps.setString(2, t.getPasswordUtente());
		int modificate = ps.executeUpdate();
		if (modificate > 0) {
			return getById(t.getIdUtente());
		}
		
		return null;
		
	}
	
	

}
