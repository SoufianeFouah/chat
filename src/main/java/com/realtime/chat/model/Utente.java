package com.realtime.chat.model;

import java.util.ArrayList;

public class Utente {
	
	private String username;
	private String passwordUtente;
	private String tipologia;
	private Integer idUtente;
	
	private ArrayList<Messaggio> elencoMessaggi = new ArrayList<Messaggio>(); 
	
	public void addMessaggio(Messaggio m) {
		elencoMessaggi.add(m);
		
	}
	
	public ArrayList<Messaggio> getElencoMessaggi() {
		return elencoMessaggi;
	}
	

	public void setElencoMessaggi(ArrayList<Messaggio> elencoMessaggi) {
		this.elencoMessaggi = elencoMessaggi;
	}

	@Override
	public String toString() {
		return "Utente [username=" + username + ", passwordUtente=" + passwordUtente + ", tipologia=" + tipologia
				+ ", idUtente=" + idUtente + "]";
	}
	
	public Utente() {
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPasswordUtente() {
		return passwordUtente;
	}
	public void setPasswordUtente(String passwordUtente) {
		this.passwordUtente = passwordUtente;
	}
	public String getTipologia() {
		return tipologia;
	}
	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}
	public Integer getIdUtente() {
		return idUtente;
	}
	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}
	
	
	

}
