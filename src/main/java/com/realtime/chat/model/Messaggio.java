package com.realtime.chat.model;

import java.time.LocalDateTime;

public class Messaggio {
	
	private Integer idMessaggio;
	private Integer utenteId;
	private String textMessaggio;
	private LocalDateTime dataMessaggio;
	

	@Override
	public String toString() {
		return "Messaggio [idMessaggio=" + idMessaggio + ", utenteId=" + utenteId + ", textMessaggio=" + textMessaggio
				+ ", dataMessaggio=" + dataMessaggio + "]";
	}
	
	public Messaggio() {
		
	}
	
	public Integer getIdMessaggio() {
		return idMessaggio;
	}
	public void setIdMessaggio(Integer idMessaggio) {
		this.idMessaggio = idMessaggio;
	}
	public Integer getUtenteId() {
		return utenteId;
	}
	public void setUtenteId(Integer utenteId) {
		this.utenteId = utenteId;
	}
	public String getTextMessaggio() {
		return textMessaggio;
	}
	public void setTextMessaggio(String textMessaggio) {
		this.textMessaggio = textMessaggio;
	}
	public LocalDateTime getDataMessaggio() {
		return dataMessaggio;
	}
	public void setDataMessaggio(LocalDateTime dataMessaggio) {
		this.dataMessaggio = dataMessaggio;
	}
	
	
}
