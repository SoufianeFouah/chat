DROP DATABASE IF EXISTS real_time_chat;
CREATE DATABASE real_time_chat;
USE real_time_chat;

CREATE TABLE utente(
	idUtente INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(150) NOT NULL UNIQUE,
    passwordUtente VARCHAR(150) NOT NULL,
    tipologia VARCHAR(10) NOT NULL,
    CONSTRAINT checkTipologia CHECK(tipologia ="Admin" OR tipologia ="Base")
);

CREATE TABLE messaggio(
	idMessaggio INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    utenteId INTEGER NOT NULL,
    textMessaggio TEXT NOT NULL,
    dataMessaggio DATETIME NOT NULL,
    FOREIGN KEY(utenteId) REFERENCES utente(idUtente) ON DELETE NO ACTION
);

INSERT INTO utente (username, passwordUtente, tipologia) VALUES
("IoanDinu","toor","Admin"),
("Soufiane","toor","Admin"),
("GiovanniP","password","Base");

SELECT * FROM utente;